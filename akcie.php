<!DOCTYPE html>
<html lang="en">

<head>
  <title>Zoznam akcií rybárskeho krúžku vo Svätom Jure</title>
  <meta charset="UTF-8">
<meta name="description" content="Akcie prebiehajúce v rámci rybárskeho krúžku vo Svätom Jure">
<meta name="keywords" content="rybársky krúžok, mladý rybár, mladí rybári, ryby, krúžok, svätý jur">
<meta name="author" content="Martin Bystriansky">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="css/my.css">
 
 
</head>
<body>

<?php
session_start();
include_once 'menu.php';
include_once 'class.php';
?>
<!-- KONIEC MENU --> 
   <!-- Zactiatok Jumbotron -->  
<?php
include_once 'jumbo.php';
?>
  <!-- Koniec jumbotron --> 

<!-- ZACIATOK OBSAHU-->
    <div class="row">   
 <?php
       $akcie = new Akcie();
      if(isset($_GET['id'])){
          $id_akcie= $_GET['id'];
          echo '<div class="col-md-12 text-center"> ';
          $akcie->ZobrazAkciu($id_akcie);
          echo '</div>';
      }
      else {
          if($_GET['p']==="z"){
              echo '<div class="col-md-3 text-center text-zeleny"> ';
              echo '<h3>Všetky akcie</h3>';
        $akcie->VypisVsetkyZazite();
        echo '</div>';
        echo '<div class="col-md-9">
            <div class="text-zlty">
        <h2>V tejto sekcii budeme zverejňovať všetky akcie, ktoré sme spolu zažili.  </h2>
        </div>
        <br>
        <p>Pokúsime sa ku každému stretnutiu rybárskeho krúžku napísať krátke 
        zodnotenie, čo sme zažili a čo sme sa prípadne naučili</p>
                </div>';
        
        
    }
    else {
        echo '<div class="col-md-3 text-center text-zeleny"> ';
        echo '<h3>Všetky akcie</h3>';
        $akcie->VypisVsetkyNasledujuce();
         echo '</div>';
       echo '<div class="col-md-9">
            <div class="text-zlty">
        <h2>V tejto sekcii budeme zverejňovať všetky akcie, ktoré pripravujeme  </h2>
        </div>
        <br>
        <p>Budeme sa snažiť akcie zverejňovať čo najskôr, aby ste sa vedeli
        pripraviť, prípadne si spraviť cez víkend voľno.
        <br>
        Radi by sme organizovali akcie častejšie a možno aj cez týždeň, 
        ale uvidíme, ako to bude s časom a ako sa Vám bude chcieť.
        </p>
                </div>';
        
     }
       
      }
      ?>
      
     </div>
 </div>
    <!-- KONIEC STPLPCA 2.2 -->
    
    
  </div>
 <!-- KONIEC OBSAHU OBSAHU-->

</div>
    <!-- FOOOTER start -->
   <?php
   include 'footer.php';
   ?>
</body>
</html>

