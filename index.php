<!DOCTYPE html>
<html lang="en">

<head>
  <title>Klub Mladých rybárov Svätý Jur</title>
  <meta charset="UTF-8">
<meta name="description" content="Stránka Klubu Mladých Rybárov vo Svätom Jure">
<meta name="keywords" content="rybársky krúžok, klub mladých rybárov, mladí rybári, ryby, deti, svätý jur">
<meta name="author" content="Martin Bystriansky">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
 <script> 
i.fb,       span.fb{     color: green; }
</script> 

   <link rel="stylesheet" href="css/my.css">
 
 
</head>
<body>

<?php
session_start();
include_once 'menu.php';
include_once 'class.php';
include_once 'jumbo.php';
?>

  <!-- KONIEC MENU --> 

<div class="row">
    <div class="col-md-12">
      <p>
        <div class="text-zlty"><h1>Stránke venovaná návštevníkom a záujemcom o rybársky krúžok vo Svätom Jure</h1></div>
        <br>
        <p>Sme radi, že ste zavítali na našu stránku.</p>
        <p><strong><div class="text-zeleny">Prečo sme vznikli? </strong></div>Rybárska krúžok vznikol
        na základe toho,že pri vode už nie je vidno žiadne deti a radi by sme 
        im tento pekný šport priblížili, sme sa rozhodli založiť takýto krúžok.
        Momentálne je účasť na krúžku <strong>bezplatná</strong>. Neradi by 
        sme deti alebo skôr rodičov finančné zaťažovali. Chceme len, aby sa deti
        zabavali a strávili trochu času na vzduchu. Dokonca na rok 2016 vybavujeme
        deťom povolenia na rybolov bezplatne. Jediné, čo bude potrebné uhradiť bude 
        členský rybársky preukaz (cca 2€).
        </p>
        
        <br>
        
        <p><div class="text-zeleny"><strong>Kto sme? </strong></div>Krúžok 
        vedie partia mladých chalanov vo veku od 23 do 30 rokov. Dvaja z nás 
        momentálne úspešne absolvovali skúšky na rybársku stráž a od roku 2016
        budú pôsobiť ako členovia rybárskej stráže. Všetci máme dlhoročné skúsenosti
        s lovom a staroslitovsťou o ryby.
        Snažíme sa, aby deti boli uvoľnené a chodili sem s radosťou. Určite nechceme
        nasadzovať tvrdý školský režim. </p>
        
        <br>
        
        <p>Na tejto hlavnej stránke budeme uvádzať všetky aktuality. V sekcii
        <strong>Galéria</strong> si budete môcť pozrieť fotky z akcií alebo inštruktážne videá.
        Sekcia <strong>Akcie</strong> bude podujatia, ktoré plánujeme uskutočniť,
        alebo sú už za nami. Tiež budeme poskytovať na stiahnutie fotky, videá 
        a tiež výučbové materiály prostredníctvom sekcie <strong>Download</strong></p>
       
     
    </div>
  <div class="row">
  <div class="col-md-12" >
      <a href="galeria.php">
       <img src="img/a.jpg" class="img-responsive img-rounded" alt="obrázok">
       <div class="text-nad-obrazkami">
            <h1>Galéria</h1>
       </div>
       </a>
 </div>
      </div>
       <div class="row">   
      <!-- KONIEC STPLPCA 1 -->
    <div class="col-md-6"> 
        <a href="akcie.php?p=z">
       <img src="img/b.jpg" class="img-responsive img-rounded" alt="obrázok">
       <div class="text-nad-obrazkami">
           <h1>Zažili sme</h1>
       </div>
       </a>
      </div> 
    
      <!-- KONIEC STPLPCA 2.1 -->
    <div class="col-md-6"> 
        <a href="akcie.php?p=c">
      <img src="img/c.jpg" class="img-responsive img-rounded" alt="obrázok">
      <div class="text-nad-obrazkami center-block">
           <h1>Čaká nás</h1>
       </div>
      </a>
       </div>
 </div>
    <!-- KONIEC STPLPCA 2.2 -->
    
    
  </div>
    <!-- KONIEC RIADKU -->

</div>
    <!-- FOOOTER start -->
 <?php
 
 include 'footer.php';
   ?>
</body>


</html>

